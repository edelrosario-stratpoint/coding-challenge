package com.blastasiachallenge.picsumgallery

import android.app.Application
import com.blastasiachallenge.picsumgallery.data.ServiceProvider
import com.blastasiachallenge.picsumgallery.data.network.repository.details.DetailRepository
import com.blastasiachallenge.picsumgallery.data.network.repository.gallery.GalleryRepository

class PicsumApp: Application() {

    val galleryRepository: GalleryRepository
        get() = ServiceProvider.provideGalleryRepository(this)

    val detailRepository: DetailRepository
        get() = ServiceProvider.provideDetailRepository(this)

}