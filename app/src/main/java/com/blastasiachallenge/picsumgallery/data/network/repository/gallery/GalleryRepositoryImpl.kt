package com.blastasiachallenge.picsumgallery.data.network.repository.gallery

import com.blastasiachallenge.picsumgallery.data.base.BaseRepository
import com.blastasiachallenge.picsumgallery.data.db.AppDatabase
import com.blastasiachallenge.picsumgallery.data.network.GalleryApiService

class GalleryRepositoryImpl(
    private val galleryApiService: GalleryApiService,
    private val appDatabase: AppDatabase
): BaseRepository(), GalleryRepository {

    override suspend fun getGalleryImages(page: Int, limit: Int) = serviceCall {

        val result = galleryApiService.getGalleryImages(page, limit)

        if (page == 1) {
            appDatabase.imageDao().deleteAllImages()
        }

        appDatabase.imageDao().upsertImages(result)

        result

    }

    override suspend fun getCachedGalleryImages() = serviceCall {
        appDatabase.imageDao().getAllImages()
    }

}