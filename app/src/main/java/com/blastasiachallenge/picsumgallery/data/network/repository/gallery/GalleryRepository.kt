package com.blastasiachallenge.picsumgallery.data.network.repository.gallery

import com.blastasiachallenge.picsumgallery.data.base.Resource
import com.blastasiachallenge.picsumgallery.domain.Image

interface GalleryRepository {

    suspend fun getGalleryImages(page:Int, limit: Int = 30): Resource<List<Image>>

    suspend fun getCachedGalleryImages(): Resource<List<Image>>
}