package com.blastasiachallenge.picsumgallery.data

import android.content.Context
import androidx.room.Room
import com.blastasiachallenge.picsumgallery.BuildConfig
import com.blastasiachallenge.picsumgallery.data.base.NoInternetInterceptor
import com.blastasiachallenge.picsumgallery.data.db.AppDatabase
import com.blastasiachallenge.picsumgallery.data.network.GalleryApiService
import com.blastasiachallenge.picsumgallery.data.network.repository.details.DetailRepository
import com.blastasiachallenge.picsumgallery.data.network.repository.details.DetailRepositoryImpl
import com.blastasiachallenge.picsumgallery.data.network.repository.gallery.GalleryRepository
import com.blastasiachallenge.picsumgallery.data.network.repository.gallery.GalleryRepositoryImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServiceProvider {

    @Volatile
    private lateinit var appDatabase: AppDatabase

    @Volatile
    private lateinit var retrofit: Retrofit

    @Volatile
    private lateinit var galleryRepository: GalleryRepository

    @Volatile
    private lateinit var detailRepository: DetailRepository


    private const val TIMEOUT = 20000//20 seconds
    private const val DATABASE_NAME = "picsum.db"

    fun provideGalleryRepository(context: Context): GalleryRepository {
        synchronized(this) {

            if (this::galleryRepository.isInitialized.not()) {
                galleryRepository = GalleryRepositoryImpl(
                    provideRetrofit(context).create(GalleryApiService::class.java),
                    provideDatabase(context)
                )
            }

            return galleryRepository

        }
    }

    fun provideDetailRepository(context: Context): DetailRepository {
        synchronized(this) {

            if (this::detailRepository.isInitialized.not()) {
                detailRepository = DetailRepositoryImpl(
                    provideDatabase(context)
                )
            }

            return detailRepository
        }
    }

    private fun provideDatabase(context: Context): AppDatabase {
        synchronized(this) {

            if (this::appDatabase.isInitialized.not()) {
                appDatabase = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    DATABASE_NAME
                ).fallbackToDestructiveMigration().build()
            }

            return appDatabase

        }
    }

    private fun provideRetrofit(context: Context): Retrofit {
        synchronized(this) {

            if (this::retrofit.isInitialized.not()) {
                retrofit = createRetrofit(context)
            }

            return retrofit

        }
    }

    private fun createRetrofit(context: Context) = Retrofit.Builder().apply {
        addConverterFactory(GsonConverterFactory.create())
        baseUrl(BuildConfig.BASE_URL)

        val client = OkHttpClient.Builder().apply {

            connectTimeout(TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
            readTimeout(TIMEOUT.toLong(), TimeUnit.MILLISECONDS)

            addInterceptor(HttpLoggingInterceptor().apply {
                setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
            })

            addInterceptor(NoInternetInterceptor(context))

        }.build()

        client(client)

    }.build()

}