package com.blastasiachallenge.picsumgallery.data.base

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}