package com.blastasiachallenge.picsumgallery.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.blastasiachallenge.picsumgallery.domain.Image
import retrofit2.http.DELETE

@Dao
abstract class ImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun upsertImages(images: List<Image>)

    @Query("SELECT * FROM Image")
    abstract suspend fun getAllImages(): List<Image>

    @Query("DELETE FROM Image")
    abstract suspend fun deleteAllImages()

    @Query("SELECT * FROM Image WHERE id=:id")
    abstract suspend fun getImage(id: String): Image

}