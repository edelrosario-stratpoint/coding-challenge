package com.blastasiachallenge.picsumgallery.data.network.repository.details

import com.blastasiachallenge.picsumgallery.data.base.Resource
import com.blastasiachallenge.picsumgallery.domain.Image

interface DetailRepository {

    suspend fun getImage(id: String): Resource<Image>

}