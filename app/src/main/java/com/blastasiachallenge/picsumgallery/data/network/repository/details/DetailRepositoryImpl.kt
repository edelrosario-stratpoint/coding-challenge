package com.blastasiachallenge.picsumgallery.data.network.repository.details

import com.blastasiachallenge.picsumgallery.data.base.BaseRepository
import com.blastasiachallenge.picsumgallery.data.db.AppDatabase

class DetailRepositoryImpl(private val appDatabase: AppDatabase): BaseRepository(), DetailRepository {

    override suspend fun getImage(id: String) = serviceCall {
        appDatabase.imageDao().getImage(id)
    }

}