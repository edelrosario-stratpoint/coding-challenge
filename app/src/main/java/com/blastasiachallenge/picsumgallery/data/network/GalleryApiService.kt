package com.blastasiachallenge.picsumgallery.data.network

import com.blastasiachallenge.picsumgallery.domain.Image
import retrofit2.http.GET
import retrofit2.http.Query

interface GalleryApiService {

    @GET("v2/list")
    suspend fun getGalleryImages(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): List<Image>

}