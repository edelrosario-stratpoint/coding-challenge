package com.blastasiachallenge.picsumgallery.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.blastasiachallenge.picsumgallery.data.db.dao.ImageDao
import com.blastasiachallenge.picsumgallery.domain.Image

@Database(
    entities = [Image::class], version = 1, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun imageDao(): ImageDao

}