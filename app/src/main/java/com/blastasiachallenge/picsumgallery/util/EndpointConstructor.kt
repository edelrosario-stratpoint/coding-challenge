package com.blastasiachallenge.picsumgallery.util

import com.blastasiachallenge.picsumgallery.BuildConfig

object EndpointConstructor {

    fun String.getPreviewUrl() = "${BuildConfig.BASE_URL}id/$this/250/400"

    fun getOriginalUrl(id: String, width: Int, height: Int) =
        "${BuildConfig.BASE_URL}id/$id/$width/$height.jpg"

}