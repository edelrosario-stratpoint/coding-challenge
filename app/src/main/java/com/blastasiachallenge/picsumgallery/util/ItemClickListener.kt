package com.blastasiachallenge.picsumgallery.util

interface ItemClickListener<T> {
    fun onItemClick(item: T)
}