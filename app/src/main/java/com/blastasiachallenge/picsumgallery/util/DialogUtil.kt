package com.blastasiachallenge.picsumgallery.util

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.blastasiachallenge.picsumgallery.R

object DialogUtil {

    fun showAlertDialog(
        context: Context,
        title: String? = null,
        message: String,
        onDismissListener: DialogInterface.OnDismissListener? = null
    ) {

        AlertDialog.Builder(context)
            .setCancelable(false)
            .setMessage(message)
            .also {

                if (title != null) {
                    it.setTitle(title)
                }

                if (onDismissListener != null) {
                    it.setOnDismissListener(onDismissListener)
                }

            }
            .setPositiveButton(context.getString(R.string.label_ok_caps), null)
            .show()

    }

    fun showConfirmationDialog(
        context: Context,
        title: String? = null,
        message: String,
        buttonPositive: String,
        buttonNegative: String,
        onClickListener: DialogInterface.OnClickListener
    ) {

        AlertDialog.Builder(context)
            .setCancelable(false)
            .setMessage(message)
            .also {

                if (title != null) {
                    it.setTitle(title)
                }

            }
            .setPositiveButton(buttonPositive, onClickListener)
            .setNegativeButton(buttonNegative, onClickListener)
            .show()

    }

}