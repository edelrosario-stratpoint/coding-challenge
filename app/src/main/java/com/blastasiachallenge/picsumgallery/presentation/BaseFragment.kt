package com.blastasiachallenge.picsumgallery.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.blastasiachallenge.picsumgallery.presentation.gallery.BaseActivity

abstract class BaseFragment<VB : ViewBinding>(
    private val setupBinding: (LayoutInflater) -> VB
) : Fragment() {

    lateinit var viewBinding: VB

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                this@BaseFragment.handleOnBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = setupBinding(inflater)

        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        observeViewModel()
        loadContent()
    }

    open fun setUpView() {}
    open fun observeViewModel() {}
    open fun loadContent() {}

    open fun handleOnBackPressed() {
        if (findNavController().navigateUp().not()) {
            requireActivity().finish()
        }
    }

    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>): T = f() as T
        }

    fun getBaseActivity() = requireActivity() as BaseActivity

}