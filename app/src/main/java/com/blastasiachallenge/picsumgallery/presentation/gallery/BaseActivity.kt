package com.blastasiachallenge.picsumgallery.presentation.gallery

import androidx.appcompat.app.AppCompatActivity
import com.blastasiachallenge.picsumgallery.PicsumApp

open class BaseActivity : AppCompatActivity() {

    fun getApp() = application as PicsumApp

}