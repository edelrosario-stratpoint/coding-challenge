package com.blastasiachallenge.picsumgallery.presentation.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.blastasiachallenge.picsumgallery.R
import com.blastasiachallenge.picsumgallery.databinding.ItemGalleryBinding
import com.blastasiachallenge.picsumgallery.domain.Image
import com.blastasiachallenge.picsumgallery.util.EndpointConstructor
import com.blastasiachallenge.picsumgallery.util.EndpointConstructor.getPreviewUrl
import com.blastasiachallenge.picsumgallery.util.ItemClickListener
import com.bumptech.glide.Glide

class GalleryImagesAdapter(private val listener: ItemClickListener<Image>): RecyclerView.Adapter<GalleryImagesAdapter.GalleryItemViewHolder>() {

    private val galleryImages: MutableList<Image> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_gallery, parent, false)
        return GalleryItemViewHolder(ItemGalleryBinding.bind(view))
    }

    override fun getItemCount() = galleryImages.size

    override fun onBindViewHolder(holder: GalleryItemViewHolder, position: Int) {

        val image = galleryImages[holder.adapterPosition]

        Glide.with(holder.itemView.context)
            .load(image.id.getPreviewUrl())
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_loading_failed)
            .centerCrop()
            .into(holder.ivPreview)

        holder.tvAuthor.text = image.author

        holder.itemView.setOnClickListener {
            listener.onItemClick(galleryImages[holder.adapterPosition])
        }

    }

    fun updateList(newList: List<Image>) {

        val diffResult = DiffUtil.calculateDiff(GalleryImageDiffUtil(newList, galleryImages))
        diffResult.dispatchUpdatesTo(this)

        galleryImages.apply {
            clear()
            addAll(newList)
        }

    }

    inner class GalleryItemViewHolder(itemBinding: ItemGalleryBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        val ivPreview = itemBinding.ivPreview
        val tvAuthor = itemBinding.tvAuthor

    }

    inner class GalleryImageDiffUtil(private val newList: List<Image>, private val oldList: List<Image>): DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldImage = oldList[oldItemPosition]
            val newImage = newList[newItemPosition]

            return oldImage.author == newImage.author &&
                    oldImage.width == newImage.width &&
                    oldImage.height == newImage.height &&
                    oldImage.url == newImage.url &&
                    oldImage.downloadUrl == newImage.downloadUrl

        }


    }

}