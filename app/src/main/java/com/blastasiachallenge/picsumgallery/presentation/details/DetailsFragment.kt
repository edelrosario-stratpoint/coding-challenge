package com.blastasiachallenge.picsumgallery.presentation.details

import androidx.fragment.app.viewModels
import com.blastasiachallenge.picsumgallery.R
import com.blastasiachallenge.picsumgallery.databinding.FragmentDetailsBinding
import com.blastasiachallenge.picsumgallery.domain.Image
import com.blastasiachallenge.picsumgallery.presentation.BaseFragment
import com.blastasiachallenge.picsumgallery.util.DialogUtil
import com.bumptech.glide.Glide

class DetailsFragment : BaseFragment<FragmentDetailsBinding>(
    FragmentDetailsBinding::inflate
) {

    private val viewModel by viewModels<DetailsViewModel> {
        viewModelFactory {
            DetailsViewModel(getBaseActivity().getApp().detailRepository)
        }
    }

    override fun setUpView() {
        super.setUpView()

        viewBinding.toolbar.setNavigationOnClickListener {
            handleOnBackPressed()
        }

    }

    override fun observeViewModel() {
        super.observeViewModel()

        viewModel.image.observe(this, {
            populate(it)
        })

        viewModel.errorMessage.observe(this, {
            showError(it)
        })

    }

    override fun loadContent() {
        super.loadContent()

        val imageId = arguments?.getString(BUNDLE_IMAGE_ID)

        if (imageId == null) {
            showError(getString(R.string.error_image_id_not_found))
        }
        else {
            viewModel.getImage(imageId)
        }

    }

    companion object {
        const val BUNDLE_IMAGE_ID = "image_id"
    }

    private fun showError(errorMessage: String) {

        DialogUtil.showAlertDialog(
            context = requireContext(),
            message = errorMessage
        ) {
            handleOnBackPressed()
        }

    }

    private fun populate(image: Image) {

        Glide.with(requireContext())
            .load(image.downloadUrl)
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_loading_failed)
            .into(viewBinding.ivFullImage)

        viewBinding.tvAuthor.text = image.author

    }

}