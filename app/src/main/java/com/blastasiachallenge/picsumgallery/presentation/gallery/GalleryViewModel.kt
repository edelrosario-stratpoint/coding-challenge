package com.blastasiachallenge.picsumgallery.presentation.gallery

import androidx.lifecycle.*
import com.blastasiachallenge.picsumgallery.data.base.Status
import com.blastasiachallenge.picsumgallery.data.network.repository.gallery.GalleryRepository
import com.blastasiachallenge.picsumgallery.domain.Image
import kotlinx.coroutines.launch

class GalleryViewModel(private val galleryRepository: GalleryRepository) : ViewModel() {

    private var page = 0

    var isApiLoading = false
    private val _dataLoading: MutableLiveData<Boolean> = MutableLiveData()
    val dataLoading: LiveData<Boolean>
        get() = _dataLoading

    private val _imageList: MutableLiveData<MutableList<Image>> = MutableLiveData()
    val imageList: LiveData<List<Image>> = _imageList.map {
        it.toList()
    }

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    fun getGalleryImages(refresh: Boolean) {

        if (!isApiLoading) {

            if (refresh) {
                page = 0
            }
            setDataLoading(true)
            page += 1

            viewModelScope.launch {

                //display cached data on first load
                if (page == 1 && !refresh) {

                    val cache = galleryRepository.getCachedGalleryImages()

                    when (cache.status) {
                        Status.SUCCESS -> {
                            _imageList.value = cache.data?.toMutableList()
                        }

                        Status.ERROR -> {
                            cache.message.let {
                                _errorMessage.value = it
                            }
                        }
                    }

                }

                val result = galleryRepository.getGalleryImages(page)

                when (result.status) {

                    Status.SUCCESS -> {

                        //clear displayed cache data on first load
                        if (page == 1) _imageList.value?.clear()

                        result.data?.let {

                            _imageList.value?.addAll(it)
                            _imageList.value = _imageList.value ?: it.toMutableList()

                        }

                    }

                    Status.ERROR -> {
                        result.message.let {
                            _errorMessage.value = it
                        }
                    }
                }

                setDataLoading(false)

            }

        }


    }

    private fun setDataLoading(isLoading: Boolean) {
        isApiLoading = isLoading
        _dataLoading.value = isApiLoading
    }

}