package com.blastasiachallenge.picsumgallery.presentation.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blastasiachallenge.picsumgallery.data.base.Status
import com.blastasiachallenge.picsumgallery.data.network.repository.details.DetailRepository
import com.blastasiachallenge.picsumgallery.domain.Image
import kotlinx.coroutines.launch

class DetailsViewModel(private val detailRepository: DetailRepository):  ViewModel() {

    private val _image: MutableLiveData<Image> = MutableLiveData()
    val image: LiveData<Image>
        get() = _image

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    fun getImage(id: String) {

        viewModelScope.launch {

            val result = detailRepository.getImage(id)

            when (result.status) {

                Status.SUCCESS -> {
                    _image.value = result.data
                }

                Status.ERROR -> {
                    result.message.let {
                        _errorMessage.value = it
                    }
                }
            }

        }

    }

}