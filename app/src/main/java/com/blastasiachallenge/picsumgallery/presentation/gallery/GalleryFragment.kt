package com.blastasiachallenge.picsumgallery.presentation.gallery

import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blastasiachallenge.picsumgallery.R
import com.blastasiachallenge.picsumgallery.databinding.FragmentGalleryBinding
import com.blastasiachallenge.picsumgallery.domain.Image
import com.blastasiachallenge.picsumgallery.presentation.BaseFragment
import com.blastasiachallenge.picsumgallery.presentation.details.DetailsFragment
import com.blastasiachallenge.picsumgallery.util.ItemClickListener

class GalleryFragment : BaseFragment<FragmentGalleryBinding>(
    FragmentGalleryBinding::inflate
), ItemClickListener<Image> {

    private val viewModel by viewModels<GalleryViewModel> {
        viewModelFactory {
            GalleryViewModel(getBaseActivity().getApp().galleryRepository)
        }
    }

    private val galleryAdapter by lazy { GalleryImagesAdapter(this) }

    private var isPullToRefresh = false

    override fun setUpView() {
        super.setUpView()

        viewBinding.toolbarContainer.toolbar.title = getString(R.string.app_name)

        viewBinding.rvGallery.apply {
            val gridLayoutManager = GridLayoutManager(context, 2)
            layoutManager = gridLayoutManager
            adapter = galleryAdapter

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    if (dy > 0) {

                        val visibleItemCount = viewBinding.rvGallery.childCount
                        val totalItemCount = gridLayoutManager.itemCount
                        val firstVisibleItem = gridLayoutManager.findFirstVisibleItemPosition()

                        if (totalItemCount >= 30) {
                            if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + 5)) {
                                loadContent()
                            }

                            //show loadmore spinner when we reach the end
                            if ((totalItemCount - visibleItemCount) <= (firstVisibleItem)) {
                                if (viewModel.isApiLoading) {
                                    viewBinding.pbLoadMore.visibility = View.VISIBLE
                                }
                            }
                        }

                    } else {
                        if (viewBinding.pbLoadMore.visibility == View.VISIBLE) {
                            viewBinding.pbLoadMore.visibility = View.GONE
                        }
                    }

                }
            })

        }

        viewBinding.srlRefresher.setOnRefreshListener {
            isPullToRefresh = true
            viewModel.getGalleryImages(true)
        }

    }

    override fun observeViewModel() {
        super.observeViewModel()

        viewModel.dataLoading.observe(this, {

            if (it) {
                if (isPullToRefresh) {
                    viewBinding.srlRefresher.isRefreshing = true
                }
                else {
                    viewBinding.pbLoadMore.visibility = View.VISIBLE
                }
            }
            else {
                viewBinding.srlRefresher.isRefreshing = false
                viewBinding.pbLoadMore.visibility = View.GONE
                isPullToRefresh = false
            }

        })

        viewModel.imageList.observe(this, {
            galleryAdapter.updateList(it)
        })

        viewModel.errorMessage.observe(this, {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        })

    }

    override fun loadContent() {
        super.loadContent()

        viewModel.getGalleryImages(false)

    }

    override fun onItemClick(item: Image) {
        findNavController().navigate(R.id.action_GalleryFragment_to_DetailsFragment, bundleOf(DetailsFragment.BUNDLE_IMAGE_ID to item.id))
    }

}