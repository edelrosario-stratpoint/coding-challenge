package com.blastasiachallenge.picsumgallery.domain

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Image(
    @PrimaryKey
    var id: String,
    var author: String,
    var width: Int,
    var height: Int,
    var url: String,
    @SerializedName("download_url")
    var downloadUrl: String
)