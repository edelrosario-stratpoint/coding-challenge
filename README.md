# BlastAsia Coding Challenge
Android Coding challenge for Stratpoint

# Project Specifications

## Code
* Semi CLEAN Architecture
* MVVM Design Pattern
* Manual Dependency Injection

## Libraries Used
* AndroidX
    * Navigation
    * Fragment KTX
    * Activity KTX
    * ViewModel
    * LiveData
    * Material
    * ConstraintLayout
    * SwipeRefreshLayout
    * CardView
* Coroutines
* Room
* Retrofit
* GSON
* Glide


Prepared by:


Ernest John Del Rosario


edelrosario@stratpoint.com